public class Pizza {

    public static int counter = 0;

    public String name;
    public int size;
    public double prise;

    public Pizza(){
        counter ++;
    }

    public Pizza(String name, int size, double prise){
        this.name = name;
        this.size = size;
        this.prise = prise;
    }


    public String toString(){

        return String.format("name %s size %d prise %f ", name, size, prise);
    }



}
